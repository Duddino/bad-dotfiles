(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("c48551a5fb7b9fc019bf3f61ebf14cf7c9cdca79bcb2a4219195371c02268f11" "3d4df186126c347e002c8366d32016948068d2e9198c496093a96775cc3b3eaa" "81c3de64d684e23455236abde277cda4b66509ef2c28f66e059aa925b8b12534" default))
 '(message-default-charset 'iso-8859-1)
 '(package-selected-packages
   '(lsp-ui csharp-mode flycheck rustic latex-preview-pane exwm-systemtray mpdmacs exwm lsp-mode sublime-themes company dracula-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil)))))

;; Disable toolbar and friends
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; Line numbers
(global-display-line-numbers-mode)

(load-theme 'spolsky)

(setq backup-directory-alist `(("." . "~/.saves")))


(use-package lsp-mode
  :ensure t
  :init
     (setq lsp-keymap-prefix "C-c l")
     (add-hook 'c-mode-hook #'lsp-deferred)
  :commands lsp)

(use-package lsp-ui :commands lsp-ui-mode)

;; Company (Auto completion)
(add-hook 'after-init-hook 'global-company-mode)

;; Ligature support
;; (use-package ligature
;;   :load-path "/home/duddino/.emacs.d/ligature.el/"
;;   :config
;;   (ligature-set-ligatures 't '("www"))
;;   (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
;;   (ligature-set-ligatures 'prog-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
;;                                      ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
;;                                      "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
;;                                      "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
;;                                      "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
;;                                      "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
;;                                      "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
;;                                      "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
;;                                      "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
;;                                      "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"))
;;   (global-ligature-mode t))

;; Rust
(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm
  (setq-local buffer-save-without-query t))

(add-hook 'c-mode-hook
            (lambda ()
;;              (push '(">=" . ?≥) prettify-symbols-alist)
	      (setq prettify-symbols-alist (append prettify-symbols-alist '(
									    ("->" . ?→)
									    ("=>" . ?⇒)
									    ("->>" . ?↠)
									    ("map" . ?↦)
									    ("!=" . ?≠)
									    ("==" . ?≡)
									    ("<=" . ?≤)
									    (">=" . ?≥)
									    ("<=<" . ?↢)
									    (">=>" . ?↣)
									    ("&&" . ?∧)
									    ("||" . ?∨)
									    ("not" . ?¬))))
	      (prettify-symbols-mode)
	      ))
(add-hook 'rustic-mode-hook
            (lambda ()
;;              (push '(">=" . ?≥) prettify-symbols-alist)
	      (setq prettify-symbols-alist (append prettify-symbols-alist '(
									    ("->" . ?→)
									    ("=>" . ?⇒)
									    ("->>" . ?↠)
									    ("map" . ?↦)
									    ("!=" . ?≠)
									    ("==" . ?≡)
									    ("<=" . ?≤)
									    (">=" . ?≥)
									    ("<=<" . ?↢)
									    (">=>" . ?↣)
									    ("&&" . ?∧)
									    ("||" . ?∨)
									    ("not" . ?¬))))
	      (prettify-symbols-mode)
	      ))


			      ;; (setq prettify-symbols-alist '(("lambda" . ?λ)
				    
			      ;; 	    (prettify-symbols-mode)
			      ;; 	    ))))
	  
