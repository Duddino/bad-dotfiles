(define-package "dracula-theme" "20210502.1743" "Dracula Theme"
  '((emacs "24.3"))
  :commit "0a76928fdb49d1cf65b10c706ae0e1bbc779effb" :authors
  '(("film42"))
  :maintainer
  '("film42")
  :url "https://github.com/dracula/emacs")
;; Local Variables:
;; no-byte-compile: t
;; End:
