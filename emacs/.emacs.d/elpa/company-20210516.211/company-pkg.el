(define-package "company" "20210516.211" "Modular text completion framework"
  '((emacs "25.1"))
  :commit "0331a7311a4697306f5f5e4312959acb794ad8cc" :authors
  '(("Nikolaj Schumacher"))
  :maintainer
  '("Dmitry Gutov" . "dgutov@yandex.ru")
  :keywords
  '("abbrev" "convenience" "matching")
  :url "http://company-mode.github.io/")
;; Local Variables:
;; no-byte-compile: t
;; End:
