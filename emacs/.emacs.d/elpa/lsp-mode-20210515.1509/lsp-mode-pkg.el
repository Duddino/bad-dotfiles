(define-package "lsp-mode" "20210515.1509" "LSP mode"
  '((emacs "26.1")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.3")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0"))
  :commit "3dc87f6111ea2439a8d974a8954dcbb683144b92" :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :keywords
  '("languages")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
