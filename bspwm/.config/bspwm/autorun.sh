#!/bin/sh

picom &
mpd &
polybar bar &
hsetroot -cover ~/wallpapers/windows.jpg
nm-applet &

setxkbmap it
# Swap escape with caps
setxkbmap -option caps:swapescape
# ~/.config/bar.sh | lemonbar

lxqt-notificationd -style kvantum-dark &

cbatticon &
emacs --daemon &
