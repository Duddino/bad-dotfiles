# Defined in - @ line 1
function ec --wraps='emacsclient -nc' --description 'alias ec=emacsclient -nc'
  emacsclient -nc $argv;
end
