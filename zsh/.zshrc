# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd
unsetopt beep
zstyle :compinstall filename '/home/duddino/.zshrc'

autoload -Uz compinit promptinit
compinit
promptinit
prompt gentoo

alias ls='ls --color=auto'
alias startx='echo "No."'
alias emacs='emacsclient -nc'
alias paru='paru --bottomup'
alias emacs='emacs -nw'
alias grep='grep --color=auto'

MAKEFLAGS="-j6"
export QT_QPA_PLATFORMTHEME=qt5ct

# insult
command_not_found_handler() {
    cat ~/other/insults | sort -R | head -1 || echo "Command not found"
}
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
source /usr/share/zsh/site-functions/zsh-autosuggestions.zsh

export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"
export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$PATH"
export EDITOR="emacs"
export BDB_CFLAGS="-I/usr/include"
