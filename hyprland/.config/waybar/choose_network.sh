#!/bin/sh

# Prompt for wifi name
NAME="$(nmcli -f SSID d wifi | tail -n +2 | sed 's/ *$//' | sort | uniq | fuzzel -d -i)"
# Prompt for wifi passwd
PASSWD="$(fuzzel -d --password --lines=0)"
if [ -z "$PASSWD" ]
then
    nmcli d wifi connect "$NAME"
else
    nmcli d wifi connect "$NAME" password "$PASSWD"
fi
