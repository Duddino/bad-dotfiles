#!/bin/sh

FZF='fuzzel -d --width=100'
PASS_DIR="$HOME/.password-store/"
pass $(find "$PASS_DIR" -name '*.gpg' | sed "s+$PASS_DIR++" | sed "s/.gpg$//" | $FZF) -c
